# TubiTui
#### A simple, lightweight, TUI-based Invidious/YouTube client

It uses...
* the [Invidious](https://github.com/iv-org/invidious) API to get data
* [MPV](https://mpv.io/) to stream video
* [Cursive](https://github.com/Gyscos/Cursive) to render the TUI


<p align="middle">
    <img src="../assets/rick.png" width=49% />
    <img src="../assets/rick_channel.png" width=49%>
</p>

## Features

* Search for YouTube videos and view information about them
* Play videos in mpv
* Download videos and their thumbnails
* View channels, playlists, and comments
* Save videos to local playlists
* Choose between Invidious instances to use

## Installation

### Dependencies
* `mpv` - [Installation](https://mpv.io/installation/)

On Linux and Mac install via your system package manager.

On Windows it is necessary to add the mpv executable to the path manually. Either do this using the GUI interface or using the command: 

`setx PATH "%PATH%;<path-to-mpv-folder>"`

It is a good idea to make a backup of your PATH variable before doing this.

### Releases

Download the latest release for your operating system from the [releases page](https://codeberg.org/777/TubiTui/releases).
Currently `.deb` packages, and portable binaries for Windows, Mac and Linux are available.

### Build from source
Before starting, [install Rust and Cargo](https://doc.rust-lang.org/cargo/getting-started/installation.html).

Firstly, clone the repository:

```
$ git clone https://codeberg.org/777/TubiTui
$ cd TubiTui/
```

And compile with `cargo`:

`$ cargo build --release`

Then run the resulting executable:

```
$ cd target/release/
$ ./tubitui
```

#### Options

If you want to use ncurses as a TUI backend instead of the default, crossterm:

`$ cargo build --release --features=ncurses-backend`

This requires ncurses to be installed on your system. (not available on Windows)

### Usage Notes
- Due to an issue with implementing DASH video streaming (the format used by YouTube for high quality video)
video quality is currently capped to 720p. There should be a workaround for this in the future.
- Config files and downloads are stored in `~/.tubitui` (Linux, Mac), or `C:\Users\<USER>\AppData\Roaming\tubitui` (Windows).