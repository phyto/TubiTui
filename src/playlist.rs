/*
    TubiTui - A lightweight, libre, TUI-based YouTube client
    Copyright (C) 2021 777 <g1t_777@protonmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.
*/

use crate::config::Config;
use crate::search::VideoResult;
use crate::utils::Errs;
use serde::Deserialize;
use std::error::Error;

fn get(id: &str, instance: &str, locale: &str) -> Result<String, reqwest::Error> {
    let response = reqwest::blocking::get(format!(
        "{}/api/v1/playlists/{}?region={}",
        instance, id, locale
    ))?
    .text()?;
    Ok(response)
}

#[derive(Deserialize, Default, Clone)]
#[serde(rename_all = "camelCase")]
pub struct PlaylistInfo {
    title: String,
    playlist_id: String,
    author: String,
    author_id: String,
    video_count: usize,
    view_count: usize,
    videos: Vec<VideoResult>,
}

impl PlaylistInfo {
    pub fn get(id: &str, conf: &Config) -> Result<PlaylistInfo, Errs> {
        let json = match get(id, conf.instance.as_str(), conf.locale.as_str()) {
            Ok(r) => r,
            Err(e) => {
                return if let Some(code) = e.status() {
                    Err(Errs::NetworkError(code.to_string()))
                } else {
                    Err(Errs::NetworkError(e.source().unwrap().to_string()))
                }
            }
        };
        let info: PlaylistInfo = match serde_json::from_str(json.as_str()) {
            Ok(r) => r,
            Err(e) => return Err(Errs::PageError(e.to_string())),
        };
        Ok(info)
    }
    pub fn title(&self) -> &String {
        &self.title
    }
    pub fn id(&self) -> &String {
        &self.playlist_id
    }
    pub fn author(&self) -> &String {
        &self.author
    }
    pub fn author_id(&self) -> &String {
        &self.author_id
    }
    pub fn views(&self) -> usize {
        self.view_count
    }
    pub fn length(&self) -> usize {
        self.video_count
    }
    pub fn videos(&self) -> &Vec<VideoResult> {
        &self.videos
    }
}
