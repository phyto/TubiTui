/*
    TubiTui - YouTube client with a Text User Interface
    Copyright (C) 2021 777 <g1t_777@protonmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.
*/

use crate::config::Config;
use crate::utils::Errs;
use serde::{Deserialize, Serialize};

fn get(
    query: &str,
    content_type: &str,
    instance: &str,
    locale: &str,
    page: usize,
) -> Result<String, reqwest::Error> {
    let response = reqwest::blocking::get(format!(
        "{}/api/v1/search?q={}&region={}&type={}&page={}",
        instance, query, locale, content_type, page
    ))?
    .text()?;
    Ok(response)
}

#[derive(Serialize, Deserialize, Clone, Debug)]
#[serde(tag = "type", rename_all = "camelCase")]
pub enum SearchResult {
    Video(VideoResult),
    Playlist(PlaylistResult),
    Channel(ChannelResult),
    Category,
    Null,
}

impl SearchResult {
    pub fn get(
        query: &str,
        content_type: &str,
        page: usize,
        conf: &Config,
    ) -> Result<Vec<SearchResult>, Errs> {
        let json = match get(
            query,
            content_type,
            conf.instance.as_str(),
            conf.locale.as_str(),
            page,
        ) {
            Ok(r) => r,
            Err(e) => {
                return if let Some(code) = e.status() {
                    Err(Errs::NetworkError(code.to_string()))
                } else {
                    Err(Errs::NetworkError(e.to_string()))
                };
            }
        };
        let vec: Vec<SearchResult> = match serde_json::from_str(json.as_str()) {
            Ok(r) => r,
            Err(e) => return Err(Errs::PageError(e.to_string())),
        };
        if vec.is_empty() {
            return Err(Errs::NoResults);
        }
        Ok(vec)
    }
}

#[derive(Serialize, Deserialize, Clone, Debug, Default)]
#[serde(rename_all = "camelCase")]
pub struct VideoResult {
    title: String,
    video_id: String,
    author: String,
    author_id: String,
    #[serde(default)]
    view_count: usize,
    #[serde(default)]
    published_text: String,
    length_seconds: usize,
}

impl VideoResult {
    pub fn title(&self) -> &String {
        &self.title
    }
    pub fn id(&self) -> &String {
        &self.video_id
    }
    pub fn author_id(&self) -> &String {
        &self.author_id
    }
    pub fn author(&self) -> &String {
        &self.author
    }
    pub fn views(&self) -> usize {
        self.view_count
    }
    pub fn published(&self) -> &String {
        &self.published_text
    }
    pub fn length(&self) -> usize {
        self.length_seconds
    }
}

#[derive(Serialize, Deserialize, Clone, Debug, Default)]
#[serde(rename_all = "camelCase")]
pub struct PlaylistResult {
    title: String,
    playlist_id: String,
    author: String,
    video_count: usize,
}

impl PlaylistResult {
    pub fn title(&self) -> &String {
        &self.title
    }
    pub fn author(&self) -> &String {
        &self.author
    }
    pub fn id(&self) -> &String {
        &self.playlist_id
    }
    pub fn length(&self) -> usize {
        self.video_count
    }
}

#[derive(Serialize, Deserialize, Clone, Debug, Default)]
#[serde(rename_all = "camelCase")]
pub struct ChannelResult {
    author: String,
    author_id: String,
    sub_count: usize,
    video_count: usize,
    description: String,
}

impl ChannelResult {
    pub fn author(&self) -> &String {
        &self.author
    }
    pub fn id(&self) -> &String {
        &self.author_id
    }
    pub fn description(&self) -> &String {
        &self.description
    }
    pub fn subscribers(&self) -> usize {
        self.sub_count
    }
    pub fn videos(&self) -> usize {
        self.video_count
    }
}
