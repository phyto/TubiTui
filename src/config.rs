/*
    Tubi-Tui - YouTube client with a Text User Interface
    Copyright (C) 2021 777 <g1t_777@protonmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.
*/

use crate::utils::determine_path;
use serde::{Deserialize, Serialize};
use std::{fs, io::BufReader};

#[derive(Serialize, Deserialize, Clone)]
pub struct Config {
    pub instance: String,
    pub locale: String,
    pub watch_pos: bool,
    pub dark_mode: bool,
}

impl Config {
    pub fn read() -> Result<Config, std::io::Error> {
        let (dir, path) = determine_path()?;
        if fs::metadata(&path).is_err() {
            fs::create_dir_all(&dir)?;
            fs::write(
                &path,
                br#"{"instance":"https://vid.puffyan.us","locale":"US","watch_pos":true,"dark_mode":false}"#,
            )?;
        }
        let file = fs::File::open(&path)?;
        let reader = BufReader::new(file);
        let conf: Config = serde_json::from_reader(reader)?;
        Ok(conf)
    }
    pub fn write(&self) -> Result<(), std::io::Error> {
        let (dir, path) = match determine_path() {
            Ok(v) => v,
            Err(e) => return Err(e),
        };
        let content = serde_json::to_string(self).unwrap();
        fs::write(path, content)?;
        fs::create_dir_all(dir + "temp")?;
        Ok(())
    }
}

impl Default for Config {
    fn default() -> Self {
        Config {
            instance: String::from("https://vid.puffyan.us"),
            locale: String::from("US"),
            watch_pos: true,
            dark_mode: false,
        }
    }
}
