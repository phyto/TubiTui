/*
    TubiTui - A lightweight, libre, TUI-based YouTube client
    Copyright (C) 2021 777 <g1t_777@protonmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.
*/

use crate::config::Config;
use crate::utils::Errs;
use serde::Deserialize;
use std::error::Error;

fn get(
    id: &str,
    instance: &str,
    locale: &str,
    continuation: Option<String>,
) -> Result<String, reqwest::Error> {
    let response = reqwest::blocking::get({
        let s = format!("{}/api/v1/comments/{}?region={}", instance, id, locale);
        if continuation.is_some() {
            s + "&continuation=" + continuation.unwrap().as_str()
        } else {
            s
        }
    })?
    .text()?;
    Ok(response)
}

#[derive(Deserialize, Default, Clone)]
#[serde(rename_all = "camelCase")]
pub struct CommentsPage {
    comments: Vec<CommentInfo>,
    continuation: Option<String>,
}

#[derive(Deserialize, Default, Clone)]
#[serde(rename_all = "camelCase")]
pub struct CommentReply {
    reply_count: usize,
    continuation: String,
}

#[derive(Deserialize, Default, Clone)]
#[serde(rename_all = "camelCase")]
pub struct CommentInfo {
    author: String,
    //author_id: String,
    is_edited: bool,
    content: String,
    published_text: String,
    like_count: usize,
    author_is_channel_owner: bool,
    replies: Option<CommentReply>,
}

impl CommentsPage {
    pub fn get(
        id: &str,
        continuation: Option<String>,
        conf: &Config,
    ) -> Result<CommentsPage, Errs> {
        let json = match get(
            id,
            conf.instance.as_str(),
            conf.locale.as_str(),
            continuation,
        ) {
            Ok(r) => r,
            Err(e) => {
                return if let Some(code) = e.status() {
                    Err(Errs::NetworkError(code.to_string()))
                } else {
                    Err(Errs::NetworkError(e.source().unwrap().to_string()))
                }
            }
        };
        let info: CommentsPage = match serde_json::from_str(json.as_str()) {
            Ok(r) => r,
            Err(e) => return Err(Errs::PageError(e.to_string())),
        };
        Ok(info)
    }
    pub fn continuation(&self) -> &Option<String> {
        &self.continuation
    }
    pub fn comments(&self) -> &Vec<CommentInfo> {
        &self.comments
    }
}

impl CommentInfo {
    pub fn author(&self) -> &String {
        &self.author
    }
    /*pub fn author_id(&self) -> &String {
        &self.author_id
    }*/
    pub fn is_edited(&self) -> bool {
        self.is_edited
    }
    pub fn content(&self) -> &String {
        &self.content
    }
    pub fn published(&self) -> &String {
        &self.published_text
    }
    pub fn likes(&self) -> usize {
        self.like_count
    }
    pub fn author_is_channel_owner(&self) -> bool {
        self.author_is_channel_owner
    }
    pub fn replies(&self) -> &Option<CommentReply> {
        &self.replies
    }
}

impl CommentReply {
    pub fn count(&self) -> usize {
        self.reply_count
    }
    pub fn continuation(&self) -> &String {
        &self.continuation
    }
}
